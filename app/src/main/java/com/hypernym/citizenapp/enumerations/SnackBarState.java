package com.hypernym.citizenapp.enumerations;

/**
 * Created by BILAL on 8-oct-2017.
 */
public enum SnackBarState {
    INITIALIZE,
    EMPTY,
    ERROR
}

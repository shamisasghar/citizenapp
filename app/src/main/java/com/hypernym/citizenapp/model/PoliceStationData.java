package com.hypernym.citizenapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PoliceStationData {
    @SerializedName("condition")
    @Expose
    private String condition;
    @SerializedName("data")
    @Expose
    private List<PoliceStationList> data = null;
    @SerializedName("error_msg")
    @Expose
    private String errorMsg;

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public List<PoliceStationList> getData() {
        return data;
    }

    public void setData(List<PoliceStationList> data) {
        this.data = data;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
}
